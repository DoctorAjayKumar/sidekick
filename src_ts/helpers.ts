/**
 * Common functions and constants that don't quite fit anywhere else
 *
 * @module
 */


/** `"ae_mainnet"` */
const NETWORK_ID_MAINNET = "ae_mainnet";
/** `"ae_uat"` */
const NETWORK_ID_TESTNET = "ae_uat";

//const URL_MAINNET = "ae_mainnet";
//const URL_TESTNET = "http";

/** `const MS = 1;` */
const MS  =    1;
/** `const SEC = 1000*MS;` */
const SEC = 1000*MS;
/** `const MIN = 60*SEC;` */
const MIN =   60*SEC;
/** `const HR = 60*MIN;` */
const HR  =   60*MIN;

/**
 * The wallet spams the window with a `connection.announcePresence`
 * event every 3 seconds. So 3 seconds is the de-facto maximum this
 * should take. So I doubled that and added one.
 *
 * `const TIMEOUT_DEF_DETECT = 7*SEC;`
 */
const TIMEOUT_DEF_DETECT  = 7*SEC;

/**
 * Instantaneous in practice
 *
 * `const TIMEOUT_DEF_CONNECT = 1*SEC;`
 */
const TIMEOUT_DEF_CONNECT = 1*SEC;


/**
 * This is instantaneous if the wallet trusts your site. Otherwise
 * it takes however long it takes your user to accept/reject the
 * request
 *
 * Set to `5*MIN`, but this is a case where you need to use some
 * discretion.
 */
const TIMEOUT_DEF_ADDRESS = 5*MIN;

/**
 * This takes however long it takes your user to accept/reject the
 * transaction. So use your discretion.
 *
 * Value is `5*MIN`
 */
const TIMEOUT_DEF_SIGN = 5*MIN;


/**
 * pf = pretty format
 */
function
pf
    (x : any)
    : string
{
    return JSON.stringify(x, undefined, 4);
}



/**
 * uf = ugly format
 */
function
uf
    (x : any)
    : string
{
    return JSON.stringify(x);
}



/**
 * `console.debug`s the message
 *
 * @internal
 */
function
snooper_console
    (msg : MessageEvent<any>)
    : void
{
    console.debug('snooper_console:', pf(msg.data));
}



/**
 * Appends pretty-formatted string of message to element with `id` of
 * `log`
 *
 * @internal
 */
function
snooper_html
    (msg : MessageEvent<any>)
    : void
{
    let s = pf(msg.data);
    const elt = document.getElementById("log");
    if (elt)
    {
        elt.innerHTML += s;
        elt.innerHTML += "\n\n\n";
    }
    else
    {
        throw new Error('no element with id "log" exists in the DOM');
    }
}



/**
 * Primitve to send a message into the browser's message queue
 */
function
post
    (msg : any)
    : void
{
    // Erlang: window ! msg.
    window.postMessage(msg, "*");
}



/**
 * Stupid hack that implements timer:sleep/1
 */
async function
sleep
    (ms : number)
    : Promise<void>
{
    return new Promise(resolve => setTimeout(resolve, ms));
}



/**
 * Adds `snooper_console` as an event listener
 */
function
snoop_console
    ()
    : void
{
    // Adds this code to the window process
    //
    // ruhseev
    //      Msg -> snooper_console(Msg)
    // end
    window.addEventListener('message', snooper_console);
}



/**
 * Adds `snooper_html` as an event listener
 */
function
snoop_html
    ()
    : void
{
    window.addEventListener('message', snooper_html);
}



/**
 * What it says
 */
async function
error_if_condition_is_false_else_block_until_lambda_returns_true
    (condition       : boolean,
     error_message   : string,
     fun             : () => boolean,
     timeout_ms      : number,
     timeout_message : string)
    : Promise<void>
{
    if (!condition)
        throw new Error(error_message);
    else
        await block_until_lambda_returns_true(fun, timeout_ms, timeout_message);
}

/**
 * eicifebulrt is Welsh for
 * "error if condition is false else block until lambda returns true"
 */
const eicifebulrt = error_if_condition_is_false_else_block_until_lambda_returns_true;


/**
 * Run the provided lambda every 10 milliseconds
 *
 * Returns a `Promise` that does not resolve until the provided
 * lambda evaluates to a truthy value
 */
async function
block_until_lambda_returns_true
    (fun             : () => boolean,
     timeout_ms      : number,
     timeout_message : string)
    : Promise<void>
{
    let tick_ms              = 10;
    let number_of_iterations = Math.floor(timeout_ms / tick_ms);
    for(let i = 1; i <= number_of_iterations; i++)
    {
        let done = fun();
        if (done)
            return;
        else
            // processors are too fast now we need affirmative action
            await sleep(tick_ms);
    }
    // we reach here if we have gotten through that entire loop
    // without being done
    throw new Error(timeout_message);
}


//-------------------------------------------------------------------
// Exports
//-------------------------------------------------------------------

export {
    NETWORK_ID_MAINNET,
    NETWORK_ID_TESTNET,
    SEC,
    MIN,
    HR,
    TIMEOUT_DEF_DETECT,
    TIMEOUT_DEF_CONNECT,
    TIMEOUT_DEF_ADDRESS,
    TIMEOUT_DEF_SIGN,
    pf,
    uf,
    post,
    sleep,
    snoop_console,
    snoop_html,
    eicifebulrt,
    error_if_condition_is_false_else_block_until_lambda_returns_true,
    block_until_lambda_returns_true
};
