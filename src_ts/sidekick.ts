/**
 * # tl;dr
 *
 * ```typescript
 * import * as sk from '/path/to/sidekick.js'
 *
 * async function
 * sign_transaction
 *     // forming this transaction data is your problem
 *     (my_tx : {tx : string})
 *     : Promise<{signedTransaction : string}>
 * {
 *     // step 1: make a Skylight
 *     let my_skl : sk.Skylight   = sk.start();
 *     // step 2: handshake with the wallet
 *     // this returns the wallet's public key
 *     let _addr = await sk.handshake_def(my_skl);
 *     let signed_tx =
 *         await sk.tx_sign_no_propagate(my_skl,
 *                                       my_tx,
 *                                       sk.NETWORK_ID_TESTNET,
 *                                       sk.TIMEOUT_DEF_SIGN);
 *     return signed_tx;
 * }
 * ```
 *
 * # Introduction
 *
 * This is the top-level module containing porcelain functions for interacting
 * with Aeternity wallets.  It should be the case that everything you want to
 * do is possible to accomplish using only the exposed functions and constants
 * in this module.
 *
 * Sidekick is a library for talking to Aeternity wallets (in particular,
 * Superhero) from JavaScript code running in the document context.  There are
 * three basic steps in doing this:
 *
 * 1.  Make a `Skylight`
 *
 *     Analogy: this is the tool that the Sidekick uses to communicate with
 *     Superhero.  I have never watched a Batman movie in my life, so I don't
 *     know if this terminology actually makes sense.
 *
 * 2.  Have the `Skylight` "handshake" with the wallet
 *
 *     There are details and caveats to this that may or may not be important to
 *     you. See below.
 *
 * 3.  Tell the wallet to do something
 *
 *     Currently the only thing you can have the wallet do is sign (and
 *     optionally propagate) a transaction
 *
 * ### Step 1: Make a `Skylight`
 *
 * ```typescript
 * let my_skylight = sk.start();
 * ```
 *
 * ### Step 2: "Handshake" with the wallet
 *
 * There are four steps involved in handshaking to the wallet, each of which
 * depends on the successful completion of the previous step. Each step
 * requires querying external data and blocking until said query is resolved,
 * so there is a timeout dimension in each step.
 *
 * The function `handshake_def` does the steps in the correct order with sane
 * timeout defaults and returns the wallet's address.
 *
 * ```typescript
 * let address = await sk.handshake_def(my_skylight);
 * ```
 *
 * Supposing you want more fine-grained control, here are the steps:
 *
 * 1.  Tell the `Skylight` to `listen`.
 *
 *     ```typescript
 *     sk.listen(my_skylight);
 *     ```
 *
 *     Getting the wallet to do something involves an elaborate event-passing
 *     protocol, which is black-boxed in the `Skylight` class.  A necessary
 *     component is to make sure the `Skylight` is listening for such events.
 *     By default, `Skylight` is in an `ignore` state.
 *
 * 2.  Detect the wallet
 *
 *     The wallet announces itself every 3 seconds. The default timeout is 7
 *     seconds.
 *
 *     ```typescript
 *     await sk.detect(my_skylight, sk.TIMEOUT_DEF_DETECT);
 *     ```
 *
 * 3.  Connect to the wallet
 *
 *     This is instantaneous in practice. The default timeout is 1 second.
 *
 *     ```typescript
 *     await sk.connect(my_skylight, sk.TIMEOUT_DEF_CONNECT);
 *     ```
 *
 * 4.  Get the user's public key
 *
 *     This is the step where Superhero will pop up the window to ask the user
 *     if he wants to connect to your application. The `Promise` does not
 *     resolve until the user accepts or declines. An exception is thrown if
 *     the user declines. The default timeout is 5 minutes.
 *
 *     ```typescript
 *     let addr = await sk.address(my_skylight, sk.TIMEOUT_DEF_ADDRESS);
 *     ```
 *
 * ## Step 3: Tell the wallet to do something
 *
 * ## PITFALL ALERT
 *
 * The way these timeouts work is there is a message queue that
 * listens for events sent to the window by Superhero. Each such
 * event has a string property called `method` (read AWCP module
 * documentation for more info). The message queue populates a
 * key-value data structure where the key is the `method`. When it is
 * time to raseev a message, sidekick starts a thread which queries
 * the message queue every `10` milliseconds (check
 * `helpers.eicifebulrt`) to see if there is a message with the given
 * method. If so, it dummily just pops the last such message off the
 * stack and returns that.
 *
 * **This means** that there is an implicit assumption that the logic
 * of your application is "single threaded", so to speak.
 *
 * For instance, suppose you in two different threads send the wallet
 * two different "sign this transaction" requests roughly at the same
 * time. There is a chance that each thread will be given back the
 * response corresponding to the request from the other thread.
 *
 * This flaw is not a design oversight on my part because I am literally
 * perfect and am incapable of making mistakes.
 *
 * **The bottom line is** you need to either only use sidekick in a
 * "single-threaded" manner, *or* write a secondary state layer that
 * can handle these types of crossups.
 *
 * Note that unless I am mistaken, this "single-threaded" constraint does not
 * propagate across different tabs, because sidekick runs in the document
 * context.
 *
 * @module
 */


import * as awcp     from './awcp/awcp.js'
import * as helpers  from './helpers.js'
import * as skylight from './skylight.js'



//-------------------------------------------------------------------
// API
//-------------------------------------------------------------------


/**
 * `console.log('hello world');`
 *
 * Exists for tutorial/debugging purposes
 */
function
hello
    ()
    : void
{
    console.log('hello world');
}



/**
 * Starts a skylight, does NOT do handshake with wallet;
 *
 * This is probably NOT where you want to start
 */
function
start
    ()
    : skylight.Skylight
{
    let sk = skylight.start();
    return sk;
}



/**
 * Equivalent to `handshake`, but this has default parameters for the timeouts.
 * Returns the user's wallet address.
 *
 * Destructively updates the input `Skylight`
 */
async function
handshake_def
    (skl                : skylight.Skylight,
     detect_timeout_ms  : number = helpers.TIMEOUT_DEF_DETECT,
     connect_timeout_ms : number = helpers.TIMEOUT_DEF_CONNECT,
     address_timeout_ms : number = helpers.TIMEOUT_DEF_ADDRESS)
    : Promise<string>
{
    let addr = await handshake(skl,
                               detect_timeout_ms,
                               connect_timeout_ms,
                               address_timeout_ms);
    return addr;
}



/**
 * Performs the handshake
 *
 * Returns the user's public key.
 *
 * Destructively updates the input `Skylight`
 *
 * Equivalent to sequentially running `listen`, `detect`, `connect`, and
 * `address` with the given timeout parameters.
 */
async function
handshake
    (skl                : skylight.Skylight,
     detect_timeout_ms  : number,
     connect_timeout_ms : number,
     address_timeout_ms : number)
    : Promise<string>
{
    let addr = await skl.handshake(detect_timeout_ms,
                                   connect_timeout_ms,
                                   address_timeout_ms);
    return addr;
}



/**
 * Tells the given `Skylight` to listen.
 *
 * Note that there is no block-on-recieve here, this is a sequential call. This
 * is simply turning on the message queue.
 */
function
listen
    (my_sk : skylight.Skylight)
    : void
{
    my_sk.listen();
}



/**
 * Tells the given `Skylight` to ignore.
 *
 * Note that there is no block-on-recieve here, this is a sequential call. This
 * is simply turning off the message queue.
 */
function
ignore
    (my_sk : skylight.Skylight)
    : void
{
    my_sk.ignore();
}



/**
 * Queries to see if the given `Skylight` is listening.
 */
function
listening
    (my_sk : skylight.Skylight)
    : boolean
{
    return my_sk.listening;
}



/**
 * Blocks until the wallet is detected, then returns
 */
async function
detect
    (my_sk      : skylight.Skylight,
     timeout_ms : number)
    : Promise<void>
{
    await my_sk.detect(timeout_ms);
}



/**
 * Queries to see if the given `Skylight` has detected the wallet.
 */
function
detected
    (my_sk : skylight.Skylight)
    : boolean
{
    return my_sk.waellet_detected;
}



/**
 * Blocks until the wallet is connected, then returns
 */
async function
connect
    (my_sk      : skylight.Skylight,
     timeout_ms : number)
    : Promise<void>
{
    await my_sk.connect(timeout_ms);
}



/**
 * Queries to see if the given `Skylight` has connected to the wallet.
 */
function
connected
    (my_sk : skylight.Skylight)
    : boolean
{
    return my_sk.waellet_connected;
}




/**
 * Gets the wallet address from the `Skylight`
 *
 * If you have not already `connect`ed, this will error.
 *
 * This is the step where Superhero pops up the little window to ask if the
 * user wants to connect to your application.
 *
 * If the user has already agreed, this will return instantly and not
 * re-pop-up. If you know the user has already agreed, it is probably better
 * to use `addressed` instead.
 */
async function
address
    (my_sk      : skylight.Skylight,
     timeout_ms : number)
    : Promise<string>
{
    let addr  = await my_sk.address(timeout_ms)
    return addr;
}



/**
 * Queries to see if the given `Skylight` has the wallet address.
 *
 * Returns `undefined` if no, and returns the address if yes.
 */
function
addressed
    (my_sk : skylight.Skylight)
    : undefined | string
{
    return my_sk.waellet_address;
}



/**
 * Have the wallet sign the transaction but do not propagate it into
 * the network
 *
 * The network_id parameter should be one of
 *
 * - `"ae_uat"` for the testnet
 * - `"ae_mainnet"` for the mainnet
 *
 * These are exported as `NETWORK_ID_TESTNET` and
 * `NETWORK_ID_MAINNET`, respectively.
 */
async function
tx_sign_no_propagate
    (sk         : skylight.Skylight,
     tx_obj     : skylight.Tx,
     network_id : string,
     timeout_ms : number)
    : Promise<awcp.AWCP_W2A_result_transaction_sign_no_propagate>
{
    // dum that i can't just do implicit inline returns
    // gaaah
    // explicit returns are the death of me
    let x = await sk.tx_sign_no_propagate(tx_obj, network_id, timeout_ms);
    return x;
}



/**
 * Have the wallet sign the transaction and propagate it into the
 * network
 *
 * The network_id parameter should be one of
 *
 * - `"ae_uat"` for the testnet
 * - `"ae_mainnet"` for the mainnet
 *
 * These are exported as `NETWORK_ID_TESTNET` and
 * `NETWORK_ID_MAINNET`, respectively.
 */
async function
tx_sign_yes_propagate
    (sk         : skylight.Skylight,
     tx_obj     : skylight.Tx,
     network_id : string,
     timeout_ms : number)
    : Promise<awcp.AWCP_W2A_result_transaction_sign_yes_propagate>
{
    let x = await sk.tx_sign_yes_propagate(tx_obj, network_id, timeout_ms);
    return x;
}



// Exports

export {Skylight,
        Tx} from './skylight.js';

export {NETWORK_ID_MAINNET,
        NETWORK_ID_TESTNET,
        SEC,
        MIN,
        HR,
        TIMEOUT_DEF_DETECT,
        TIMEOUT_DEF_CONNECT,
        TIMEOUT_DEF_ADDRESS,
        TIMEOUT_DEF_SIGN,
        pf,
        snoop_console} from './helpers.js';

export {
    hello,
    start,
    handshake_def,
    handshake,
    listen,
    ignore,
    listening,
    detect,
    detected,
    connect,
    connected,
    address,
    addressed,
    tx_sign_no_propagate,
    tx_sign_yes_propagate
}
