/**
 * Skylight logic
 *
 * A skylight is the thing you use to talk to a superhero
 *
 * By default, Skylight is autistic and only does exactly what you
 * tell it to do.  Making a new Skylight only populates the state
 * record with the default values. Doesn't even attempt to talk to
 * Superhero.
 *
 * In other words, the default behavior is "do what I say". You
 * probably want "do what I mean" behavior. This is where Skylight
 * does all the things you would normlly expect it do do, which is
 * establish a connection with the Superhero wallet and get the
 * user's public key.
 *
 * In that case, you probably want to do something like this
 *
 *      import * as skylight from './skylight.js'
 *
 *      async function main()
 *      {
 *          // dwim = do what I mean
 *          let my_skylight = await skylight.dwim();
 *          console.log(my_skylight);
 *      }
 *
 * @module
 */


import * as awcp         from './awcp/awcp.js';
import * as msgr         from './awcp/msgr.js';
import * as helpers      from './helpers.js';

const NETWORK_ID="ae_uat";

type Tx = {tx: string};


// process definition
class Skylight
{
    //---------------------------------------------------------------
    // STATE
    //---------------------------------------------------------------

    next_message_id   : number             = 1;
    msgr              : msgr.MsgR          = msgr.start();
    listening         : boolean            = false;
    waellet_detected  : boolean            = false;
    waellet_connected : boolean            = false;
    waellet_address   : undefined | string = undefined;



    //---------------------------------------------------------------
    // API
    //---------------------------------------------------------------

    /**
     * "do what I mean"
     *
     * - `listen`
     * - `detect` the wallet
     * - `connect` to the wallet
     * - wait for the `address` to the wallet
     * - return the address
     */
    async handshake
        (detect_timeout_ms  : number,
         connect_timeout_ms : number,
         address_timeout_ms : number)
        : Promise<string>
    {
        this.listen();
        await this.detect(detect_timeout_ms);
        await this.connect(connect_timeout_ms);
        await this.address(address_timeout_ms);
        return this.waellet_address as string;
    }


    /**
     * listen
     */
    listen
        ()
        : void
    {
        this.msgr.listen();
        this.listening = true;
    }



    /**
     * ignore
     */
    ignore
        ()
        : void
    {
        this.msgr.ignore();
        this.listening = false;
    }



    /**
     * block until waellet detected
     */
    async detect
        (timeout_ms : number)
        : Promise<void>
    {
        if (this.waellet_detected)
            return;
        else
        {
            await this.msgr.connection_announcePresence(timeout_ms);
            this.waellet_detected = true;
        }
    }



    /**
     * block until waellet connected
     */
    async connect
        (timeout_ms : number)
        : Promise<void>
    {
        if (this.waellet_connected)
            return;
        else
        {
            await this.really_connect(timeout_ms);
            this.waellet_connected = true;
        }
    }

    /** @internal */
    async really_connect
        (timeout_ms : number)
        : Promise<void>
    {
        // The angle brackets are to fix TypeScript's retarded faggy
        // fake weak ass type inference.
        //
        // Basically if I leave them out, it complains because
        //
        // - it infers from the TYPE of `this.msgr.connection.open`
        //   that the first argument to this function must have TYPE
        //   `"connection.open"`
        // - it assigns the VALUE `"connection.open"` the TYPE
        //   `string`
        // - it then bitches, because it can only type-infer upwards
        //   in the type hierarchy; that is, it can't infer that
        //   something of TYPE `string` is of TYPE
        //   `"connection.open"`
        // - so adding the angle brackets is fine I guess because it
        //   makes things a bit more explicit (which is almost never
        //   a bad thing)
        // - still stupid that the type inference has this weird flaw
        let a2w_msg =
            this.mk_a2w_msg<"connection.open", awcp.AWCP_A2W_params_connection_open>
                           ("connection.open",
                            // These parameters should be instance
                            // variables, but alas
                            {name: "Skylight",
                             // this version field is a protocol
                             // version
                             version: 1//,
                             //networkId: NETWORK_ID
                             });
        await this.msgr.connection_open(a2w_msg, timeout_ms);
    }



    /**
     * get public key
     */
    async address
        (timeout_ms : number)
        : Promise<string>
    {
        if (this.waellet_address)
            return this.waellet_address;
        else
        {
            let addr = await this.really_address(timeout_ms);
            this.waellet_address = addr;
            return addr;
        }
    }

    /** @internal */
    async really_address
        (timeout_ms : number)
        : Promise<string>
    {
        let a2w_msg =
            this.mk_a2w_msg<"address.subscribe", awcp.AWCP_A2W_params_address_subscribe>
                           ("address.subscribe",
                            {type: "subscribe",
                             value: "connected"});
        let w2a_msg = await this.msgr.address_subscribe(a2w_msg, timeout_ms);

        // contents of w2a_msg.data.result are going to be an object
        // that looks like this
        //
        //  {
        //      "subscription": [
        //          "connected"
        //      ],
        //      "address": {
        //          "current": {
        //              "ak_2Wsa8iAmAm917evwDEZjouvPUXKx2nUv5Uz8e8oNXTDfDXnMRN": {}
        //          },
        //          "connected": {}
        //      }
        //  }
        //
        // Why it's formatted that way is beyond me. But that's the
        // way it is. So to get the address we have to use the
        // following dumb hack:
        let addr = Object.keys(w2a_msg.data.result.address.current)[0];
        return addr;
    }



    /**
     * send a transaction to the wallet, and return the signed
     * transaction (do not propagate)
     */
    async tx_sign_no_propagate
        (tx         : Tx,
         network_id : string,
         timeout_ms : number)
        : Promise<awcp.AWCP_W2A_result_transaction_sign_no_propagate>
    {
        if(!(this.waellet_address))
            throw new Error("Do not have the user's wallet address; try my_skylight.connect_dwim() first");
        else
        {
            let result = await this.really_transact_noprop(tx, network_id, timeout_ms);
            return result;
        }
    }

    /** @internal */
    async really_transact_noprop
        (tx_obj     : Tx,
         network_id : string,
         timeout_ms : number)
        : Promise<awcp.AWCP_W2A_result_transaction_sign_no_propagate>
    {
        let tx_str = tx_obj.tx;
        // This returnSigned parameter apparently controls whether
        // Superhero attempts to send the transaction into the
        // blockchain (returnSigned = false), or simply signs the
        // transaction and sends it back to you (returnSigned = true)
        let tx_params = {tx           : tx_str,
                         // fucking retarded gay fake ass faggy fake
                         // type inference
                         returnSigned : true as true,
                         // this really does need to be here or
                         // superhero crashes with "invalid network
                         // id"
                         //
                         // you might naively think that if it's just
                         // signing the transaction and not
                         // propagating it, it doesn't need to know
                         // the network id. But empirically, it does.
                         //
                         // We will find out more about this when we
                         // implement Jaeck Russell
                         networkId    : network_id};
        let a2w_msg =
            this.mk_a2w_msg<"transaction.sign", awcp.AWCP_A2W_params_transaction_sign_no_propagate>
                           ("transaction.sign", tx_params);
        let response = await this.msgr.transaction_sign_no_propagate(a2w_msg, timeout_ms);
        //console.log('response:', response);
        let result = response.data.result;
        return result;
    }



    /**
     * send a transaction to the wallet, and have the wallet sign and
     * propagate it
     */
    async tx_sign_yes_propagate
        (tx         : Tx,
         network_id : string,
         timeout_ms : number)
        : Promise<awcp.AWCP_W2A_result_transaction_sign_yes_propagate>
    {
        if(!(this.waellet_address))
            throw new Error("Do not have the user's wallet address; try my_skylight.connect_dwim() first");
        else
        {
            let result = await this.really_transact(tx, network_id, timeout_ms);
            return result;
        }
    }

    /** @internal */
    async really_transact
        (tx_obj     : Tx,
         network_id : string, 
         timeout_ms : number)
        : Promise<awcp.AWCP_W2A_result_transaction_sign_yes_propagate>
    {
        let tx_str = tx_obj.tx;
        // This returnSigned parameter apparently controls whether
        // Superhero attempts to send the transaction into the
        // blockchain (returnSigned = false), or simply signs the
        // transaction and sends it back to you (returnSigned = true)
        let tx_params = {tx           : tx_str,
                         //returnSigned : true,
                         // fucking retarded gay fake ass faggy fake
                         // type inference
                         returnSigned : false as false,
                         networkId    : network_id};
        let a2w_msg =
            this.mk_a2w_msg<"transaction.sign", awcp.AWCP_A2W_params_transaction_sign_yes_propagate>
                           ("transaction.sign", tx_params);
        let response = await this.msgr.transaction_sign_yes_propagate(a2w_msg, timeout_ms);
        let result = response.data.result;
        return result;
    }



    //---------------------------------------------------------------
    // INTERNALS
    //---------------------------------------------------------------

    /**
     * - make a message
     * - is stateful, increments the message id
     *
     * @internal
     */
    mk_a2w_msg
        <method_s,
         params_t>
        (method : method_s,
         params : params_t)
        : awcp.Window_A2W_Requ<method_s, params_t>
    {
        // increment message id to guarantee uniqueness
        let this_message_id = this.next_message_id;
        this.next_message_id += 1;

        // Once again, the type inference is retarded. If I just put
        // "type": "to_waellet" in the object, it gets mad because
        // that's a string, and the type must be one of "to_waellet"
        // or "to_aepp".
        //
        // really typescript
        //
        // Anyway, `as` tells the typechecker what the type is
        //
        // apparently the same thing happens with "2.0"
        //
        // ayfkm
        //
        // Alright I gotta go like report this as a bug. This is so
        // fucking stupid
        //
        // message
        let msg = {"type": ("to_waellet" as "to_waellet"),
                   "data": {"jsonrpc" : ("2.0" as "2.0"),
                            "id"      : this_message_id,
                            "method"  : method,
                            "params"  : params}};

        return msg;
    }
}



// API
function
start
    ()
    : Skylight
{
    return new Skylight();
}



// exports
export type {
    Skylight,
    Tx
}

export {
    start
}
