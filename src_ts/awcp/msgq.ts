/**
 * Message Queue (Block-on-recieve (receive? raseev.)) implementation
 *
 * This implementation is specialized to this specific application,
 * but the idea can work in general.
 *
 * Originally, this was implemented as the Skylight logic. I wanted to
 * factor out the "message queue" and "block on recieve" functionality
 * from the actual core skylight logic
 *
 * Quoth The Dear Founder explaining this ridiculous program
 * structure
 *
 * > Working out dumdum js pointer/messaging logic
 * >
 * > the skylight is the thing that sidekick uses to talk to superhero
 * >
 * > so the skylight needs to black-box away some internal state. a
 * > process is the ideal abstraction to deal with this, but we live in
 * > hell so we get to use objects instead.
 * >
 * > the first thing the skylight needs to do is let superhero announce
 * > itself and record that it has talked to superhero
 * >
 * > JS doesn't have "block on recieve" or anything like that so i thought
 * > that I would basically do this
 * >
 * > ```
 * > class Skylight
 * > {
 * >     listen()
 * >     {
 * >         window.addListener('message', this.listener);
 * >     }
 * >
 * >     ignore()
 * >     {
 * >         window.removeListener('message', this.listener);
 * >     }
 * >
 * >     listener(msg)
 * >     {
 * >         this.do_something_with(msg);
 * >     }
 * > }
 * > ```
 * >
 * > Of course that doesn't work because we live in hell. basically here
 * > was the problem: listener() gets executed IN THE CONTEXT OF THE
 * > WINDOW despite this being an instance method
 * >
 * > so I was getting an error message like "`this.do_something_with` is
 * > not callable". Note that I was NOT getting the error message "this
 * > does not have property do_something_with" (which is what the error
 * > would be in say Python). Noooooo
 * >
 * > instead I was confused for a solid few minutes because I kept saying
 * > "yes this does, what are you talking about" and thinking I had made a
 * > typo somewhere.
 * >
 * > alas, no, my error was not a typo. my error was assuming that
 * > javascript behaved in a way that makes sense. which is usually the
 * > problem.
 * >
 * > note that we CAN'T just do this
 * >
 * > ```
 * > listener(skylight, msg)
 * > {
 * >     skylight.do_something_with(msg);
 * > }
 * >
 * > class Skylight
 * > {
 * >     listen()
 * >     {
 * >         window.addListener('message', (msg) => listener(this, msg));
 * >     }
 * >
 * >     ignore()
 * >     {
 * >         window.removeListener('message', (msg) => listener(this, msg));
 * >     }
 * >
 * > }
 * > ```
 * >
 * > for two reasons:
 * >
 * > 1. if you add an inline lambda as a listener, you can't remove it, so
 * >    ignore() wouldn't work.  because the javascript gods record the
 * >    POINTER to the function, and so the two inline lambdas, despite
 * >    being identical code, point to different memory locations.
 * >
 * >    Quoth https://developer.mozilla.org/en-US/docs/Web/API/EventTarget/addEventListener
 * >
 * >    > Note: If a particular anonymous function is in the list of event
 * >    > listeners registered for a certain target, and then later in the
 * >    > code, an identical anonymous function is given in an
 * >    > addEventListener call, the second function will also be added to
 * >    > the list of event listeners for that target.
 * >    >
 * >    > Indeed, anonymous functions are not identical even if defined
 * >    > using the same unchanging source-code called repeatedly, even if
 * >    > in a loop.
 * >    >
 * >    > Repeatedly defining the same unnamed function in such cases can
 * >    > be problematic. (See Memory issues, below.)
 * >
 * > 2. there's a second layer of jogritude to `this`, which is that,
 * >    again, the function is executed IN THE CONTEXT OF THE WINDOW. This
 * >    isn't really jogritude. On the contrary, this is a rare example of
 * >    javascript behaving in a way that makes at least kind of a little
 * >    bit of sense.
 * >
 * >    `this` in JS is a bit like self/0 in Erlang.
 * >
 * > So in order to work around all of this, I came up with the following basic logic:
 * >
 * > ```
 * > listener(skylight, msg)
 * > {
 * >     skylight.do_something_with(msg);
 * > }
 * >
 * > class Skylight
 * > {
 * >     constructor()
 * >     {
 * >         // grab pointer to this using hack
 * >         const pointer_to_this = this;
 * >         this.ls =
 * >             function(evt)
 * >             {
 * >                 listener(pointer_to_this, evt);
 * >             }
 * >     }
 * >
 * >     listen()
 * >     {
 * >         window.addListener('message', this.ls);
 * >     }
 * >
 * >     ignore()
 * >     {
 * >         window.removeListener('message', this.ls);
 * >     }
 * >
 * > }
 * > ```
 * >
 * > and this seems to work
 * >
 * > still dumb but whatever
 * >
 * > Everything that isn't Erlang is wrong
 * >
 * > Bye
 * >
 * > The Founder loves you
 *
 * Important to remember that TypeScript is structurally typed: which
 * means that the type system provides some degree of guarantee that
 * some field exists, but the type is not necessarily exhaustive.
 *
 * @module
 */


import * as helpers from '../helpers.js'

type WindowMsg_W2A =
    {type : "to_waellet" | "to_aepp",
     data : {method: string}};


function
listener
    (msgq : MsgQ,
     evt  : MessageEvent<WindowMsg_W2A>)
    : void
{
    // `evt.data` will look something like this:
    //
    //
    //      {
    //          "type": "to_aepp",
    //          "data": {
    //              "jsonrpc": "2.0",
    //              "id": 2,
    //              "method": "address.subscribe",
    //              "result": {
    //                  "subscription": [
    //                      "connected"
    //                  ],
    //                  "address": {
    //                      "current": {
    //                          "ak_2Wsa8iAmAm917evwDEZjouvPUXKx2nUv5Uz8e8oNXTDfDXnMRN": {}
    //                      },
    //                      "connected": {}
    //                  }
    //              }
    //          }
    //      }
    //
    //
    // or maybe this:
    //
    //      {
    //          "type": "to_waellet",
    //          "data": {
    //              "jsonrpc": "2.0",
    //              "id": 1,
    //              "method": "connection.open",
    //              "params": {
    //                  "name": "Example wallet",
    //                  "version": 1,
    //                  "networkId": "ae_uat"
    //              }
    //          }
    //      }
    //
    //
    // this checks the "type" field. If it matches, it gets sent back to the
    // type handler.
    //
    //      function listener(msgq: MsgQ, evt: MessageEvent<EvtData>) : void
    //      {
    //          let evt_type = evt.data.type as string;
    //          let evt_data = evt.data.data as EvtDataData;
    //
    //          let event_is_for_us = (evt_type === "to_aepp")
    //          if (event_is_for_us)
    //              msgq.handle(evt_data);
    //          else
    //              return;
    //      }
    //
    // note this won't crash if the field doesn't exist, it will just
    // be "undefined"
    let msg             = evt.data;
    let event_is_for_us = ("to_aepp" === msg.type)

    if (event_is_for_us)
        msgq.handle(msg);
    else
        return;
}


class MsgQ
{
    /**
     * The queue is a hashmap #{Method := FullMessage}
     *
     * WHY THE MESSAGE QUEUE IS AN OBJECT
     *
     * really it is `#{method:string := Array<EvtDataData>};`
     *
     * the reason this needs to be an object:
     *
     * let's start by acknowledging that the natural structure here
     * is an array. This was my first instinct: just use an array
     * here and then, in `msgq.rcv("foobar")`, loop over all of the
     * messages in the queue, and grab the one that matches "foobar"
     *
     * the problem with that approach is twofold:
     *
     * 1.  It is a poor match for the problem domain.
     *
     *     Superhero spams the message queue with "announcePresence".
     *     We could add "selective ignore" (and probably will down
     *     the line), but it's not a good idea to expect someone
     *     using the MsgQ class to remember to selectively ignore
     *     "announcePresence", only AFTER the presence has already
     *     been announced.
     *
     *     What would then happen with the loop approach is, if the
     *     end-user leaves the page open overnight, is hundreds of
     *     thousands of "connection.announcePresence" messages will
     *     appear in the queue. there may then be 3 or 4 recievers
     *     that are constantly looping through hundreds of thousands
     *     of irrelevant messages
     *
     *     I will probably have to add special handling for
     *     "connection.announcePresence" just due to memory issues.
     *
     *     So, like, by default it would ignore
     *     "connection.announcePresence" but if the programmer
     *     specifically asks to recieve it, then listen for it, wait
     *     until it shows up, then ignore it. That's adding a lot of
     *     complexity up-front into an abstraction I'm not 100%
     *     committed to yet. So that can wait.
     *
     *     The much bigger reason is...
     *
     * 2.  It is a poor match for the language.
     *
     *     Javascript has a very nice "push/pop" idiom for
     *     destructively grabbing the last element in an array:
     *
     *     ```js
     *     > obj = {}
     *     {}
     *     > obj.a = []
     *     []
     *     > obj.a.push("hello")
     *     1
     *     > obj.a.push("world")
     *     2
     *     > obj.a
     *     [ 'hello', 'world' ]
     *     > x = obj.a.pop()
     *     'world'
     *     > obj.a
     *     [ 'hello' ]
     *     > x
     *     'world'
     *     ```
     *
     *     Javascript does not have a nice idiom for "delete a
     *     specific element from the array, but also don't delete
     *     duplicate elements".
     *
     *     Equality in Javascript is a nebulous concept. Trying to
     *     code this manually seems likely to anger the javascript
     *     demons.
     *
     *     Additionally... and I hate bringing up performanth... but
     *     hashmap lookup is O(1), and looping is O(n)... so there's
     *     that.
     */
    queue     : object                                    = {}    ;
    listening : boolean                                   = false ;
    ls        : (_ : MessageEvent<WindowMsg_W2A>) => void         ;


    constructor
        ()
    {
        const pointer_to_this = this;
        this.ls =
            function (evt)
            {
                listener(pointer_to_this, evt);
            }
    }



    //---------------------------------------------------------------
    // API
    //
    // TODO: add listen/1 and ignore/1
    //---------------------------------------------------------------


    listen
        ()
        : void
    {
        window.addEventListener('message', this.ls);
        this.listening = true;
    }



    ignore
        ()
        : void
    {
        window.removeEventListener('message', this.ls);
        this.listening = false;
    }


    /**
     * "block on recieve" (not quite but close)
     *
     * In the event that there are no matching messages in the queue, **and**
     * we are not listening, an `Error` is thrown
     */
    // FUXME(dak): should also have an id field.... AHHHHHHHHH BUT NO
    // THIS VIOLATES THE PUSH/POP IDIOM... GODDAMN IT
    // alright think about that
    async rcv
        <return_type>
        (method     : string,
         timeout_ms : number)
        : Promise<return_type>
    {
        // silly pointer hack again
        let this_ptr = this;
        // lambda that must return true to unblock
        let queue_has_messages_we_want =
                function()
                {
                    return this_ptr.queue_has_message_with_method(method)
                }

        // error if condition is false
        // else block until lambda returns true
        await helpers.eicifebulrt(
                // sanity check:
                (this_ptr.listening || queue_has_messages_we_want()),
                // sanity failure message:
                "message queue is not listening and there are no matching messages in the queue",
                // lazily evaluated unblocking condition:
                queue_has_messages_we_want,
                // timeout (milliseconds)
                timeout_ms,
                // timeout failure message:
                `timeout failure: message queue has no messages with method ${method} after ${timeout_ms} milliseconds`
            );

        // ok cool there's a message there
        // let's grab it

        // Have to use `as` here because TypeScript is dumb
        //
        // Literally thinks that objects are not indexable by
        // strings. Which is just false. That's literally what
        // objects are in this clown show of a retard language.
        //
        // src_ts/awcp/msgq.ts:420:19 - error TS7053: Element implicitly has an 'any' type because expression of type 'string' can't be used to index type '{}'.
        //   No index signature with a parameter of type 'string' was found on type '{}'.
        //
        // 420         let msg = this.queue[method].pop();
        //                       ~~~~~~~~~~~~~~~~~~
        //
        //
        // Found 1 error in src_ts/awcp/msgq.ts:420
        //
        // Yes they can, TypeScript. THAT'S WHAT OBJECTS ARE YOU
        // RETARD PROGRAM
        //
        // @ts-ignore retarded fake and gay string indexing warning
        let msg = this.queue[method].pop();

        // if the message has the property `error`, then it was a
        // failure, and we are going to throw an error with that
        // error's message
        if(msg.data.error)
            throw new Error(msg.data.error.message);
        else
            return msg;
    }



    //---------------------------------------------------------------
    // INTERNALS
    //---------------------------------------------------------------

    handle
        (msg: WindowMsg_W2A)
        : void
    {
        this.add_to_queue(msg);
    }



    /**
     * add to queue:
     *
     *  - if no such key exists, then make singleton array
     *  - if key exists, then push
     */
    add_to_queue
        (msg: WindowMsg_W2A)
        : void
    {
        let method = msg.data.method;
        // @ts-ignore implicit any warning
        let key_exists = !!(this.queue[method]);

        // if this is a connection.announcePresence method, we only
        // want one in the queue, so we will ignore it
        // FUXME (dak) this logic really doesn't belong here, it
        // belongs in msgr. think about refactor
        if (method === "connection.announcePresence" && this.queue_has_message_with_method("connection.announcePresence"))
        {
            return;
        }
        else if (key_exists)
        {
            // @ts-ignore implicit any  warning
            (this.queue[method]).push(msg);
        }
        else
        {
            let arr = [msg];
            // @ts-ignore implicit any warning
            this.queue[method] = arr;
        }
    }



    /**
     * test if queue has message with method
     */
    queue_has_message_with_method
        (method: string)
        : boolean
    {
        // explanation:
        //
        //      > obj = {}
        //      {}
        //      > obj.key_exists_but_is_empty = []
        //      []
        //      > obj.key_dne
        //      undefined
        //      > !!obj.key_exists_but_is_empty
        //      true
        //      > !!obj.key_dne
        //      false
        //      > !!obj["key_exists_but_is_empty"]
        //      true
        //      > !!obj["key_dne"]
        //      false
        //      > !!obj."key_exists_but_is_empty"
        //      !!obj."key_exists_but_is_empty"
        //            ^^^^^^^^^^^^^^^^^^^^^^^^^
        //
        //      Uncaught SyntaxError: Unexpected string
        //      > method = "key_exists_but_is_empty"
        //      'key_exists_but_is_empty'
        //      > obj.method
        //      undefined
        //      > obj[method]
        //      []
        // @ts-ignore implicit any  warning
        let key_exists = !!(this.queue[method]);
        // dne = does not exist
        let key_dne    = !key_exists;
        // note: we can't just test for equality against empty array
        // wtf
        //
        //      > obj[method] === []
        //      false
        //      > obj[method] == []
        //      false
        //      > y = obj[method]
        //      []
        //      > z = []
        //      []
        //      > y == z
        //      false
        //      > y === z
        //      false
        //
        // I guess we have to test the length of the array and see if
        // it's zero
        //
        // of course this will type error if the key doesn't exist
        // lovely
        //
        //      > bad_method = "key_dne"
        //      'key_dne'
        //      > obj[bad_method]
        //      undefined
        //      > obj[method]
        //      []
        //      > obj[method].length
        //      0
        //      > obj[bad_method].length
        //      Uncaught TypeError: Cannot read property 'length' of undefined
        //
        // okay jesus
        //
        // if you've forgot
        //
        //  - we're trying to ascertain if the queue has a message of
        //    a given method in need a recipient
        //  - the queue is an object that maps method to an array of
        //    messages awaiting recipients
        //  - there's two possibilities where we return 'false' here:
        //      1. the queue has no key matching `method` (queue has
        //         NEVER been sent such a message)
        //      2. the queue has a key matching `method` but the
        //         queue is empty (queue has been sent such a message
        //         in the past, but all of them were consumed)
        //
        // alright, so I guess the logic is
        //
        // - if the key doesn't exist
        //      return false
        // - if the key exists
        //      - return false if the array is empty
        //      - else return true
        //
        // if the key doesn't exist, we've never recieved a message
        // like this
        if (key_dne)
            return false;
        else
        {
            // so now the key exists, let's check the queue
            // corresponding to that key
            // @ts-ignore implicit any  warning
            let messages_in_the_queue_that_are_of_the_given_method = this.queue[method];
            let said_messages_array_is_empty =
                (0 === messages_in_the_queue_that_are_of_the_given_method.length);

            // again, we're trying to determine if there are messages
            // so if the array is empty, we return false;
            let there_are_messages = !said_messages_array_is_empty;
            return there_are_messages;
            // jesus christ
        }
    }
}

function
start
    ()
    : MsgQ
{
    return new MsgQ;
}

export type {
    WindowMsg_W2A,
    MsgQ
}
export {
    start
}
